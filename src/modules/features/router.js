const express = require("express");
const path = require("path");

const fs = require("fs");
const fetch = require("node-fetch");
let featuresJson = require("../../../public/databases/features.json");
const router = express.Router();
router.get("/list", function (req, res) {
  res.send(featuresJson);
});

router.get("/code_push", function (req, res) {
  var myHeaders = new fetch.Headers();
  myHeaders.append("accept", "application/json");
  myHeaders.append("X-API-Token", "97956803d80defa051963568d18921845adab491");
  const owner = req.query?.owner || "tiendung221992-gmail.com";
  const namePushIos = req.query?.ios || "KK-IOS";
  const namePushAos = req.query?.aos || "KK-AOS";
  const env = req.query?.env || "Staging";
  const getIos = () => {
    return new Promise((resolve, reject) => {
      fetch(
        `https://api.appcenter.ms/v0.1/apps/${owner}/${namePushIos}/deployments/${env}/releases`,
        {
          method: "GET",
          headers: myHeaders,
          redirect: "follow",
        }
      )
        .then((response) => response.json())
        .then((response) => {
          resolve(response);
        })
        .catch(reject);
    });
  };
  const getAos = () => {
    return new Promise((resolve, reject) => {
      fetch(
        `https://api.appcenter.ms/v0.1/apps/${owner}/${namePushAos}/deployments/${env}/releases`,
        {
          method: "GET",
          headers: myHeaders,
          redirect: "follow",
        }
      )
        .then((response) => response.json())
        .then((response) => {
          resolve(response);
        })
        .catch(reject);
    });
  };
  Promise.all([getIos(), getAos()]).then((_res) => {
    res.send({ ios: _res?.[0], aos: _res?.[0] });
  });
  //
});

router.post("/edit", function (req, res) {
  const newData = featuresJson?.map?.((_item) => {
    if (_item?.featureId === req?.body?.featureId) {
      return req?.body;
    }
    return _item;
  });
  if (newData)
    fs.writeFile(
      path.join(__dirname, "../../../public/databases/features.json"),
      JSON.stringify(newData, null, 2),
      (err) => {
        if (err) throw err;
        res.send(newData);
      }
    );
});

module.exports = router;
