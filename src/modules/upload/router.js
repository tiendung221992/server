const express = require("express");
const app = express();
const path = require("path");

const router = express.Router();
const upload = require("./uploadMiddleware");
const Resize = require("./Resize");
router.get("/", function (req, res) {
  res.sendFile(__dirname + "/index.html");
});
// router.post('/post', upload.single('image'), async function (req, res) {
//     // folder upload
//     const imagePath = path.join(__dirname, '../../../public/images');
//     // call class Resize
//     const fileUpload = new Resize(imagePath);
//     if (!req.file) {
//         res.status(401).json({error: 'Please provide an image'});
//     }
//     const filename = await fileUpload.save(req.file.buffer);

//     return res.status(200).json({ name: filename });
// });

router.post("/post", upload.single("myFile"), async (req, res, next) => {
  const fileUpload = path.join(__dirname, "../../../public/images");
  const file = req.file;
  if (!file) {
    const error = new Error("Please upload a file");
    error.httpStatusCode = 400;
    return next(error);
  }
  const filename = await fileUpload.save(req.file.buffer);

  console.log("filename", filename);
  res.send(file);
});

module.exports = router;
