const express = require("express");
const app = express();
const path = require("path");
const router = express.Router();
const multer = require("multer");
router.get("/", function (req, res) {
  res.sendFile(__dirname + "/index.html");
});

// SET STORAGE
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "public/files");
  },
  filename: function (req, file, cb) {
    console.log("file",file)
    cb(
      null,
      file.originalname
      // file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    );
  },
});

var upload = multer({ storage: storage });

router.post("/upload", upload.single("myFile"), (req, res, next) => {
  const file = req.file;
  if (!file) {
    const error = new Error("Please upload a file");
    error.httpStatusCode = 400;
    return next(error);
  }
  res.send(file);
});

module.exports = router;
