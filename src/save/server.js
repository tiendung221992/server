// call all the required packages
const express = require("express");
const bodyParser = require("body-parser");

const fs = require("fs");
const app = express();
var path = require("path");
const cors = require('cors');
app.use(cors({
    origin: 'http://localhost:2001'
}));
app.use(cors({
  methods: ['GET','POST','DELETE','UPDATE','PUT','PATCH']
}));

app.use(express.static(__dirname+'/public'));
app.use(bodyParser.urlencoded({ extended: true }));

// SET STORAGE
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "public/images");
  },
  filename: function (req, file, cb) {
    console.log("file",file)
    cb(
      null,
      file.originalname
      // file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    );
  },
});

// var storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//     cb(null, 'uploads/')
//   },
//   filename: function (req, file, cb) {
//     cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
//   }
// })


//Uploading photo
app.post("/uploadphoto", upload.single("myImage"), (req, res) => {
  var img = fs.readFileSync(req.file.path);
  var encode_image = img.toString("base64");
  // Define a JSONobject for the image attributes for saving to database

  var finalImg = {
    contentType: req.file.mimetype,
    image: new Buffer(encode_image, "base64"),
  };
  db.collection("quotes").insertOne(finalImg, (err, result) => {
    console.log(result);

    if (err) return console.log(err);

    console.log("saved to database");
    res.redirect("/");
  });
});

//Get List Photo
app.get("/photos", (req, res) => {
  db.collection("mycollection")
    .find()
    .toArray((err, result) => {
      const imgArray = result.map((element) => element._id);
      console.log(imgArray);

      if (err) return console.log(err);
      res.send(imgArray);
    });
});

//Get  Photo By id
app.get("/photo/:id", (req, res) => {
  var filename = req.params.id;

  db.collection("mycollection").findOne(
    { _id: ObjectId(filename) },
    (err, result) => {
      if (err) return console.log(err);

      res.contentType("image/jpeg");
      res.send(result.image.buffer);
    }
  );
});

app.get("/get_mini_apps", cors(), function (req, res) {
  res.send({ name: "121", age: 17 });
});

// ROUTES
app.get("/", function (req, res) {
  res.sendFile(__dirname + "/src/web/index.html");
});
// app.get('/', async function (req, res) {
//   await res.render('/src/web/index');
// });
app.listen(3001, () => console.log("Server started on port 3001"));
