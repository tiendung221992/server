const express = require("express");
const bodyParser = require("body-parser");
const path = require("path")
const app = express();
const multer = require("multer");
const port = process.env.PORT || 2001;
const uploadRouter = require("./src/modules/upload/router");
const uploadFileRouter = require("./src/modules/upload-file/router");
const uploadMiniApps= require("./src/modules/miniapps/router");
const uploadFeatures= require("./src/modules/features/router");
// const uploadLogs= require("./src/modules/logs/router");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname+'/public'));
const cors=require("cors");
const corsOptions ={
   origin:'*', 
   credentials:true,            //access-control-allow-credentials:true
   optionSuccessStatus:200,
}

// _# START_API_ROUTE
app.use(cors(corsOptions)) 
// app.use(express.static("public"));
app.set("view engine", "ejs");
app.use("/upload", uploadRouter);
app.use("/upload-file", uploadFileRouter);
app.use("/miniapps", uploadMiniApps);
app.use("/features", uploadFeatures);
// app.use("/logs", uploadLogs);
// _## END_API_ROUTE


// _# START_LIVE_DEMO
// app.use(express.static(path.join(__dirname, 'live')));
// app.get('/*', function(req,res) {
// 		res.sendFile(path.join(__dirname, 'live', 'index.html'));
// });
// _## END_LIVE_DEMO

// app.use(express.static(path.join(__dirname, 'build')));
// app.get('/live/', function (req, res) {
//   res.sendFile(__dirname + "/src/live/index.html");
// });

app.get("/", function (req, res) {
  res.sendFile(__dirname + "/src/web/index.html");
});

app.get("/store", function (req, res) {
  res.sendFile(__dirname + "/src/web/store/index.html");
});
// app.get("/upload", function (req, res) {
//   res.sendFile(__dirname + "/src/modules/upload/index.html");
//   // res.render("/upload");
// });
app.listen(port, function () {
  console.log("Server is running on PORT", port);
});
